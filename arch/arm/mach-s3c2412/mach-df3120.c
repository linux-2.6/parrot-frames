/* linux/arch/arm/mach-s3c2412/mach-vstms.c
 *
 * (C) 2009 Jeroen Domburg <jeroen@spritesmods.com>
 * (C) 2009 Michel Pollet <buserror@gmail.com>
 *
 * Derived from mach-vstms.c - (C) 2006 Thomas Gleixner <tglx@linutronix.de>
 * Derived from mach-smdk2413.c - (C) 2006 Simtec Electronics
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/interrupt.h>
#include <linux/list.h>
#include <linux/timer.h>
#include <linux/init.h>
#include <linux/serial_core.h>
#include <linux/platform_device.h>
#include <linux/io.h>
#include <linux/gpio.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/nand.h>
#include <linux/mtd/nand_ecc.h>
#include <linux/mtd/partitions.h>
#include <linux/gpio_keys.h>
#include <linux/mmc/host.h>
#include <linux/input.h>

#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/irq.h>

#include <mach/hardware.h>
#include <asm/setup.h>
#include <asm/irq.h>
#include <asm/mach-types.h>

#include <plat/regs-serial.h>
#include <mach/regs-gpio.h>
#include <mach/regs-lcd.h>

#include <mach/idle.h>
#include <mach/fb.h>

#include <plat/iic.h>
#include <plat/nand.h>
#include <plat/udc.h>
#include <plat/mci.h>

#include <plat/s3c2410.h>
#include <plat/s3c2412.h>
#include <plat/clock.h>
#include <plat/devs.h>
#include <plat/cpu.h>
#include <plat/mci.h>
#include <mach/regs-gpio.h>
#include <linux/pwm_backlight.h>


static struct map_desc df3120_iodesc[] __initdata = {
};

#define UCON (S3C2410_UCON_DEFAULT | S3C2412_UCON_PCLK2)
#define ULCON (S3C2410_LCON_CS8 | S3C2410_LCON_PNONE | S3C2410_LCON_STOPB)
#define UFCON (S3C2410_UFCON_RXTRIG8 | S3C2410_UFCON_FIFOMODE)

static struct s3c2410_uartcfg df3120_uartcfgs[]  = {
	[0] = {
		.hwport	     = 0,
		.flags	     = 0,
		.ucon	     = UCON,
		.ulcon	     = ULCON,
		.ufcon	     = UFCON,
	},
	[1] = {
		.hwport	     = 1,
		.flags	     = 0,
		.ucon	     = UCON,
		.ulcon	     = ULCON,
		.ufcon	     = UFCON,
	},
	[2] = {
		.hwport	     = 2,
		.flags	     = 0,
		.ucon	     = UCON,
		.ulcon	     = ULCON,
		.ufcon	     = UFCON,
	}
};

static struct gpio_keys_button df3120_buttons[] = {
	{
		.gpio		= S3C2410_GPF(3),
		.code		= KEY_LEFT,
		.desc		= "Left",
		.active_low	= 1,
	},
	{
		.gpio		= S3C2410_GPF(4),
		.code		= KEY_ENTER,
		.desc		= "Select",
		.active_low	= 1,
	},
	{
		.gpio		= S3C2410_GPF(2),
		.code		= KEY_RIGHT,
		.desc		= "Right",
		.active_low	= 1,
	}
};

static struct gpio_keys_platform_data df3120_button_data  = {
	.buttons	= df3120_buttons,
	.nbuttons	= ARRAY_SIZE(df3120_buttons),
};


static struct platform_device df3120_button_device = {
	.name		= "gpio-keys",
	.id		= -1,
	.dev		= {
		.platform_data	= &df3120_button_data,
	}
};

static struct s3c24xx_mci_pdata df3120_mmc_cfg  = {
   .gpio_detect   = S3C2410_GPF(7),
   .gpio_wprotect = S3C2410_GPG(4),
   .set_power     = NULL,
   .ocr_avail     = MMC_VDD_32_33|MMC_VDD_33_34,
};


static struct mtd_partition df3120_nand_part[] = {
	[0] = {
		.name	= "u-boot",
		.size	= SZ_256K,
		.offset	= 0,
	},
	[1] = {
		.name	= "u-boot-env",
		.offset = SZ_256K,
		.size	= SZ_32K,	/* two erase blocks */
	},
	[2] = {
		.name	= "Kernel",
		.offset = SZ_256K + SZ_32K,
		.size	= 0x200000,
	},
	[3] = {
		.name	= "root",
		.offset	= SZ_256K + SZ_32K + 0x200000,
		.size	= MTDPART_SIZ_FULL,
	},
};

static struct s3c2410_nand_set df3120_nand_sets[] = {
	[0] = {
		.name		= "NAND",
		.nr_chips	= 1,
		.nr_partitions	= ARRAY_SIZE(df3120_nand_part),
		.partitions	= df3120_nand_part,
	},
};


/* choose a set of timings which should suit most 512Mbit
 * chips and beyond.
*/

static struct s3c2410_platform_nand df3120_nand_info = {
	.tacls		= 20,
	.twrph0		= 60,
	.twrph1		= 20,
	.nr_sets	= ARRAY_SIZE(df3120_nand_sets),
	.sets		= df3120_nand_sets,
	.ignore_unset_ecc = 1,
};

static void df3120_udc_pullup(enum s3c2410_udc_cmd_e cmd)
{
	switch (cmd) {
	case S3C2410_UDC_P_ENABLE :
		s3c2410_gpio_setpin(S3C2410_GPB(8), 1);
		break;
	case S3C2410_UDC_P_DISABLE :
		s3c2410_gpio_setpin(S3C2410_GPB(8), 0);
		break;
	case S3C2410_UDC_P_RESET :
		break;
	default:
		break;
	}
}

static struct s3c2410_udc_mach_info df3120_udc_cfg  = {
	.udc_command		= df3120_udc_pullup,
	.vbus_pin		= S3C2410_GPG(1),
	.vbus_pin_inverted	= 0,
};



/* LCD timing and setup */

#define LCD_XRES	 (320)
#define LCD_YRES	 (240)
#define LCD_LEFT_MARGIN  (20)
#define LCD_RIGHT_MARGIN (38)
#define LCD_LOWER_MARGIN (5)
#define LCD_UPPER_MARGIN (15)
#define LCD_VSYNC	 (3)
#define LCD_HSYNC	 (30)

#define LCD_REFRESH	 (60)

#define LCD_HTOT (LCD_HSYNC + LCD_LEFT_MARGIN + LCD_XRES + LCD_RIGHT_MARGIN)
#define LCD_VTOT (LCD_VSYNC + LCD_LOWER_MARGIN + LCD_YRES + LCD_UPPER_MARGIN)

static struct s3c2410fb_display df3120_display[] = {
	[0] = {
		.width		= LCD_XRES,
		.height		= LCD_YRES,
		.xres		= LCD_XRES,
		.yres		= LCD_YRES,
		.left_margin	= LCD_LEFT_MARGIN,
		.right_margin	= LCD_RIGHT_MARGIN,
		.upper_margin	= LCD_UPPER_MARGIN,
		.lower_margin	= LCD_LOWER_MARGIN,
		.hsync_len	= LCD_HSYNC,
		.vsync_len	= LCD_VSYNC,

		.pixclock	= (1000000000000LL /
				   (LCD_REFRESH * LCD_HTOT * LCD_VTOT)),

		.bpp		= 16,
		.type		= (S3C2410_LCDCON1_TFT16BPP |
				   S3C2410_LCDCON1_TFT),

		.lcdcon5	= (S3C2410_LCDCON5_FRM565 |
                                   S3C2410_LCDCON5_INVVCLK |
				   S3C2410_LCDCON5_INVVLINE |
				   S3C2410_LCDCON5_INVVFRAME),

//				   S3C2410_LCDCON5_INVVDEN)
//				   S3C2410_LCDCON5_PWREN),
	},
};

#define S3C2410_GPCCON_MASK(x)	(3 << ((x) * 2))
#define S3C2410_GPDCON_MASK(x)	(3 << ((x) * 2))

static struct s3c2410fb_mach_info df3120_fb_info = {
	.displays	 = df3120_display,
	.num_displays	 = ARRAY_SIZE(df3120_display),
	.default_display = 0,


	/* Enable VD[2..7], VD[10..15], VD[18..23] and VCLK, syncs, VDEN
	 * and disable the pull down resistors on pins we are using for LCD
	 * data. */
#if 0
	.gpcup		= (0xf << 1) | (0x3f << 10),

	.gpccon		= (S3C2410_GPC1_VCLK   | S3C2410_GPC2_VLINE |
			   S3C2410_GPC3_VFRAME | S3C2410_GPC4_VM |
			   S3C2410_GPC10_VD2   | S3C2410_GPC11_VD3 |
			   S3C2410_GPC12_VD4   | S3C2410_GPC13_VD5 |
			   S3C2410_GPC14_VD6   | S3C2410_GPC15_VD7),

	.gpccon_mask	= (S3C2410_GPCCON_MASK(1)  | S3C2410_GPCCON_MASK(2)  |
			   S3C2410_GPCCON_MASK(3)  | S3C2410_GPCCON_MASK(4)  |
			   S3C2410_GPCCON_MASK(10) | S3C2410_GPCCON_MASK(11) |
			   S3C2410_GPCCON_MASK(12) | S3C2410_GPCCON_MASK(13) |
			   S3C2410_GPCCON_MASK(14) | S3C2410_GPCCON_MASK(15)),

	.gpdup		= (0x3f << 2) | (0x3f << 10),

	.gpdcon		= (S3C2410_GPD2_VD10  | S3C2410_GPD3_VD11 |
			   S3C2410_GPD4_VD12  | S3C2410_GPD5_VD13 |
			   S3C2410_GPD6_VD14  | S3C2410_GPD7_VD15 |
			   S3C2410_GPD10_VD18 | S3C2410_GPD11_VD19 |
			   S3C2410_GPD12_VD20 | S3C2410_GPD13_VD21 |
			   S3C2410_GPD14_VD22 | S3C2410_GPD15_VD23),

	.gpdcon_mask	= (S3C2410_GPDCON_MASK(2)  | S3C2410_GPDCON_MASK(3) |
			   S3C2410_GPDCON_MASK(4)  | S3C2410_GPDCON_MASK(5) |
			   S3C2410_GPDCON_MASK(6)  | S3C2410_GPDCON_MASK(7) |
			   S3C2410_GPDCON_MASK(10) | S3C2410_GPDCON_MASK(11)|
			   S3C2410_GPDCON_MASK(12) | S3C2410_GPDCON_MASK(13)|
			   S3C2410_GPDCON_MASK(14) | S3C2410_GPDCON_MASK(15)),
#endif
};

static int df3120_bl_init(struct device *dev)
{
	int ret=0;

	s3c2410_gpio_cfgpin(S3C2410_GPB(2), S3C2410_GPB2_TOUT2);
	ret = gpio_request(S3C2410_GPB(2), "lcd backlight enable");
	if (!ret)
		ret = gpio_direction_output(S3C2410_GPB(2), 0);

	/* TFT reset line up */
	s3c2410_gpio_cfgpin(S3C2410_GPC(6), S3C2410_GPIO_OUTPUT);
	s3c2410_gpio_setpin(S3C2410_GPC(6), 1);
	/* Enable LCM */
	s3c2410_gpio_cfgpin(S3C2410_GPG(12), S3C2410_GPIO_OUTPUT);
	s3c2410_gpio_cfgpin(S3C2410_GPG(13), S3C2410_GPIO_OUTPUT);
	s3c2410_gpio_cfgpin(S3C2410_GPG(14), S3C2410_GPIO_OUTPUT);
	s3c2410_gpio_cfgpin(S3C2410_GPG(15), S3C2410_GPIO_OUTPUT);
	s3c2410_gpio_setpin(S3C2410_GPG(12), 1);
	s3c2410_gpio_setpin(S3C2410_GPG(13), 1);
	s3c2410_gpio_setpin(S3C2410_GPG(14), 1);
	s3c2410_gpio_setpin(S3C2410_GPG(15), 1);

	return ret;
}

static int df3120_bl_notify(int brightness)
{
	if (brightness == 0) {
		s3c2410_gpio_cfgpin(S3C2410_GPB(2), S3C2410_GPIO_OUTPUT);
		s3c2410_gpio_setpin(S3C2410_GPB(2), 0);
	} else {
		s3c2410_gpio_cfgpin(S3C2410_GPB(2), S3C2410_GPB2_TOUT2);
		gpio_set_value(S3C2410_GPB(2), 770-brightness);
	}
	return brightness;
}

static void df3120_bl_exit(struct device *dev)
{
	gpio_free(S3C2410_GPB(2));
}

static struct platform_pwm_backlight_data df3120_backlight_data = {
	.pwm_id		= 2,
	.max_brightness	= 770,
	.dft_brightness	= 300,
	.pwm_period_ns	= 1000000000 / (100 * 256 * 20),
	.init		= df3120_bl_init,
	.notify		= df3120_bl_notify,
	.exit		= df3120_bl_exit,
};

static struct platform_device df3120_backlight_device = {
	.name		= "pwm-backlight",
	.dev		= {
		.parent	= &s3c_device_timer[2].dev,
		.platform_data = &df3120_backlight_data,
	},
};


static struct platform_device *df3120_devices[] __initdata = {
	&s3c_device_ohci,
	&s3c_device_wdt,
	&s3c_device_i2c0,
	&s3c_device_iis,
	&s3c_device_rtc,
	&s3c_device_lcd,
	&s3c_device_nand,
	&s3c_device_sdi,
	&s3c_device_timer[1],
	&s3c_device_timer[2],
	&s3c_device_usbgadget,
	&df3120_backlight_device,
	&df3120_button_device,
};

static void __init df3120_fixup(struct machine_desc *desc,
				  struct tag *tags, char **cmdline,
				  struct meminfo *mi)
{
	if (tags != phys_to_virt(S3C2410_SDRAM_PA + 0x100)) {
		mi->nr_banks=1;
		mi->bank[0].start = 0x30000000;
		mi->bank[0].size = SZ_8M;
		mi->bank[0].node = 0;
	}
}

static void __init df3120_map_io(void)
{
	s3c_device_nand.dev.platform_data = &df3120_nand_info;
	s3c_device_sdi.dev.platform_data = &df3120_mmc_cfg;

	s3c24xx_init_io(df3120_iodesc, ARRAY_SIZE(df3120_iodesc));
	s3c24xx_init_clocks(26000000);
	s3c24xx_init_uarts(df3120_uartcfgs, ARRAY_SIZE(df3120_uartcfgs));
}

static void __init df3120_init(void)
{
	s3c_i2c0_set_platdata(NULL);
	s3c24xx_fb_set_platdata(&df3120_fb_info);
	s3c24xx_udc_set_platdata(&df3120_udc_cfg);
	platform_add_devices(df3120_devices, ARRAY_SIZE(df3120_devices));
}

MACHINE_START(DF3120, "DF3120")
	.phys_io	= S3C2410_PA_UART,
	.io_pg_offst	= (((u32)S3C24XX_VA_UART) >> 18) & 0xfffc,
	.boot_params	= S3C2410_SDRAM_PA + 0x100,

	.fixup		= df3120_fixup,
	.init_irq	= s3c24xx_init_irq,
	.init_machine	= df3120_init,
	.map_io		= df3120_map_io,
	.timer		= &s3c24xx_timer,
MACHINE_END
